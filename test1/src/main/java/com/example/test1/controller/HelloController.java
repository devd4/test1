package com.example.test1.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping
	public String hello() {
		
		return"<center><h1>Welcome to Git!!!</h1></center>";
	}

}
